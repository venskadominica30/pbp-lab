import 'package:flutter/material.dart';
import 'screens/home.dart';
import 'screens/profile.dart';
import 'screens/notes.dart';
import 'screens/todo.dart';
import 'screens/blogs.dart';
import 'screens/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Joeurnnal',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Joeurnnal 좋은날'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
   List<Map<String, Object>> _pages = [];
  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  void initState() {
    _pages = [
      {
        'page': const HomeScreen(),
        'title': 'Joeurnnal 좋은날',
      },
      {
        'page': const ProfileScreen(),
        'title': 'Profile',
      },
      {
        'page': const NotesScreen(),
        'title': 'Notes',
      },
      {
        'page': const TodoScreen(),
        'title': 'To Do',
      },
      {
        'page': const BlogsScreen(),
        'title': 'Public Blog',
      },
      {
        'page': const MessageScreen(),
        'title': 'Message',
      },
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(_pages[_selectedPageIndex]['title'] as String),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
      
      child: new Container(
      color: const Color.fromRGBO(145, 107, 191,1),
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              const DrawerHeader(
                child:
                    Text('Joeurnnal 좋은날', style: TextStyle(color: Colors.white, fontSize: 50)),
                    decoration: BoxDecoration(
                    color: Color.fromRGBO(47, 47, 47, 1),
                    ),
              ),
              TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(),
                      ),
                      labelText: 'Search...',
                    ),
                  ),
              ListTile(
                title: const Text('Home', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(0);
                },
              ),
              ListTile(
                title: const Text('Profile', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(1);
                },
              ),
              ListTile(
                title: const Text('Notes', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(2);
                },
              ),
              ListTile(
                title: const Text('To Do', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(3);
                },
              ),
              ListTile(
                title: const Text('Blogs', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(4);
                },
              ),
              ListTile(
                title: const Text('Message', style: TextStyle(color: Colors.white, fontSize: 18)),
                onTap: () {
                  _selectPage(5);
                },
              ),
            ],
          ),
        ),
      ),
      body: (_pages[_selectedPageIndex]['page']) as Widget,
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
    );
  }
}