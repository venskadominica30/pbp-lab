Apakah perbedaan antara JSON dan XML?
-XML merupakan bahasa markup berbasis teks yang berkonsentrasi dalam mendefinisikan elemen sedangkan, JSON hanyalah format data yang digunakan untuk pertukaran data yang jauh lebih mudah bagi komputer untuk mengurai data yang sedang dikirim. Dalam penyimpanannya, data XML disimpan sebagai tree structure dan data JSON disimpan dengan pasangan key value seperti map. Dalam proses pengolahannya, XML dapat melakukan pemrosesan dan pemformatan dokumen dan objek namun JSON tidak dapat melakukan pemrosesan atau perhitungan apapun. Transmisi data yang lebih lambat pada XML disebabkan oleh besar dan lambatnya penguraian, sedangkan pada JSON transfer datanya lebih cepat karena ukuran filenya yang sangat kecil apalagi saat dilakukan oleh mesin JavaScript. XML mendukung banyak tipe data yang kompleks seperti chart, bagan, dan tipe data non-primitif lainnya. Sedangkan, JSON hanya mendukung string, angka, array boolean, dan objek yang hanya berisi tipe data primitif.

Kelebihan dan kekurangan XML dan JSON :

-JSON-

Kelebihan :

-Mudah untuk dipahami
-Sintaksnya sangat mudah
-Mendukung semua browser 
-Dapat diurai dalam JavaScript menggunakan fungsi eval()

Kekurangan :

-Tidak adanya dukungan name space menjadikan ekstesibilitas JSON buruk
-Terbatasnya dukungan tool pengembangan

-XML-

Kelebihan :

-Cepatnya pertukaran data antar platform yang berbeda.
-XML memisahkan data dari HTML
-Proses perubahan platform disederhanakan oleh XML

Kekurangan :

-Membutuhkan aplikasi pemrosesan
-Sintaksnya suka membingungkan dan berlebihan
-Tidak ada dukungan tipe data intrinsik

Apakah perbedaan antara HTML dan XML?
-Beberapa perbedaan yang dimiliki antara HTML dan XML adalah sebagai berikut. XML memiliki fokus untuk melakukan transfer data, sedangkan HTML berfokus kepada penyajian data. Pada HTML besar kecilnya huruf serta kalimat sangat berpengaruh, namun HTML tidak peka terhadap Case Sensitive tersebut. Tag penutup menjadi perhatian yang sangat ketat pada XML dan tidak menjadi masalah pada HTML. Tag pada XML juga dapat dikembangkan kembali, namun pada HTML tagnya sedikit terbatas.

-HTML-

Kelebihan :

-Mudah dimengerti
-Memiliki sintaks yang sederhana
-Dapat menggunakan tag yang berbeda untuk membuat halaman web
-Terdapat pilihan untuk menggunakan berbagai warna, objek, dan tata letak
-Interface browser dokumen HTML mudah dibuat

Kekurangan :

-Tidak memiliki pemeriksaan terhadap sintaks dan struktur
-Pertukaran dan penyimpanan data tidak cocok pada HTML
-Bukan bahasa yang dapat dikembangkan dan sangat tidak stabil, sehingga HTML tidak berorientasi terhadap objek 


-XML-

Kelebihan :

-XML membuat dokumen yang dapat diangkut melintasi sistem dan aplikasi
-XML menyederhanakan proses pembuatan platform
-Pertukaran data dengan berbagai platform dapat dilakukan dengan cepat

Kekurangan :

-Membutuhkan aplikasi pemrosesan
-Tidak mengizinkan user untuk membuat tag-nya
-Sintaksnya mirip dengan format transmisi data alternatif berbasis teks


Referensi : 
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://blogs.masterweb.com/perbedaan-xml-dan-html/